﻿//using System.Collections.Generic;
//using System.Linq;
//using Microsoft.AspNet.Mvc;
//using Moq;
//using Xunit;
//using Zeroprox.Domain.Abstract.IRepositories;
//using Zeroprox.Domain.Entities;
//using Zeroprox.Web.Controllers;

//// ToDo: Finish testing mastery controller
//// ToDo: Wait for moq support for dnxcore50
//// ToDo: Also remember to test all other controllers in the future

//namespace Zeroprox.Tests.ControllerTests
//{
//    public class MasteryControllerTests
//    {
//        // Test index
//        [Fact]
//        public void can_get_all_masteries_from_index()
//        {
//            // arrange
//            var mock = new Mock<IGenericRepository<Mastery>>();
//            mock.Setup(m => m.GetAll()).Returns(new[]
//            {
//                new Mastery {MasteryId = 1, Rank = "Noob"},
//                new Mastery {MasteryId = 2, Rank = "Pleb"},
//                new Mastery {MasteryId = 3, Rank = "Kappa"}
//            });

//            var target = new MasteriesController(mock.Object);

//            // act
//            var vr = target.Index() as ViewResult;
//            var result = ((IEnumerable<Mastery>) vr.ViewData.Model).ToArray();

//            // assert
//            Assert.Equal(result.Length, 3);
//            Assert.Equal("Noob", result[0].Rank);
//            Assert.Equal("Pleb", result[1].Rank);
//            Assert.Equal("Kappa", result[2].Rank);
//        }

//        // Test Details
//        [Fact]
//        public void verify_details_returns_404_if_mastery_is_null()
//        {
//            // arrange
//            var mock = new Mock<IGenericRepository<Mastery>>();
//            mock.Setup(m => m.GetById(It.IsAny<int?>())).Returns<Mastery>(null);

//            var target = new MasteriesController(mock.Object);

//            // act
//            var httpStatusCodeResult = target.Details(null) as HttpStatusCodeResult;

//            // assert
//            Assert.NotNull(httpStatusCodeResult);
//            Assert.Equal(404, httpStatusCodeResult.StatusCode);
//        }

//        [Fact]
//        public void verify_details_returns_404_if_mastery_not_found()
//        {
//            // arrange
//            var mock = new Mock<IGenericRepository<Mastery>>();
//            mock.Setup(m => m.GetById(It.IsAny<int?>())).Returns<Mastery>(null);

//            var target = new MasteriesController(mock.Object);

//            // act
//            var httpStatusCodeResult = target.Details(1) as HttpStatusCodeResult;

//            // assert
//            Assert.NotNull(httpStatusCodeResult);
//            Assert.Equal(404, httpStatusCodeResult.StatusCode);
//        }

//        [Fact]
//        public void verify_details_returns_mastery()
//        {
//            // arrange
//            var mock = new Mock<IGenericRepository<Mastery>>();
//            mock.Setup(m => m.GetById(It.IsAny<int?>())).Returns((int id) => new Mastery
//            {
//                MasteryId = id,
//                Rank = "Noob"
//            });

//            var target = new MasteriesController(mock.Object);

//            // act
//            var vr = target.Details(1) as ViewResult;
//            var result = vr.ViewData.Model as Mastery;

//            // assert
//            Assert.NotNull(vr);
//            Assert.NotNull(result);
//            Assert.Equal(1, result.MasteryId);
//            Assert.Equal("Noob", result.Rank);
//        }

//        // Test Create
//        [Fact]
//        public void verify_create_mastery_redirects_to_error()
//        {
//            // arrange
//            var mock = new Mock<IGenericRepository<Mastery>>();
//            mock.Setup(m => m.Create(It.IsAny<Mastery>()));
//            var mastery = new Mastery {MasteryId = 1};

//            var target = new MasteriesController(mock.Object);

//            // Model state error
//            target.ModelState.AddModelError("Error", "Rank is required");

//            // act
//            var result = target.Create(mastery) as ViewResult;

//            // assert
//            Assert.NotNull(result);
//            Assert.Equal("Create", result.ViewName);
//        }

//        [Fact]
//        public void verify_create_can_create_mastery()
//        {
//            // arrange
//            var mock = new Mock<IGenericRepository<Mastery>>();
//            mock.Setup(m => m.Create(It.IsAny<Mastery>())).Verifiable();

//            var mastery = new Mastery
//            {
//                MasteryId = 1,
//                Rank = "Noob"
//            };

//            var target = new MasteriesController(mock.Object);

//            // act
//            var result = target.Create(mastery) as RedirectToActionResult;

//            // assert
//            Assert.NotNull(result);
//            Assert.Equal("Index", result.ActionName);

//            mock.Verify();
//        }

//        // Test Edit
//        [Fact]
//        public void verify_edit_returns_404_if_mastery_not_found()
//        {
//            // arrange
//            var mock = new Mock<IGenericRepository<Mastery>>();
//            mock.Setup(m => m.GetById(It.IsAny<int?>())).Returns<Mastery>(null);

//            var target = new MasteriesController(mock.Object);

//            // act
//            var httpStatusCodeResult = target.Edit(1) as HttpStatusCodeResult;

//            // assert
//            Assert.NotNull(httpStatusCodeResult);
//            Assert.Equal(404, httpStatusCodeResult.StatusCode);
//        }

//        [Fact]
//        public void can_render_edit_view()
//        {
//            // arrange
//            var mock = new Mock<IGenericRepository<Mastery>>();
//            mock.Setup(m => m.GetById(It.IsAny<int?>())).Returns((int id) => new Mastery
//            {
//                MasteryId = id,
//                Rank = "Noob"
//            });

//            var target = new MasteriesController(mock.Object);

//            // act
//            var vr = target.Edit(1) as ViewResult;
//            var result = vr.ViewData.Model as Mastery;

//            // assert
//            Assert.NotNull(vr);
//            Assert.NotNull(result);
//            Assert.Equal(1, result.MasteryId);
//            Assert.Equal("Noob", result.Rank);
//        }

//        // Test Delete
//        [Fact]
//        public void delete_post_action_returns_redirectAction_to_index()
//        {
//            // arrange
//            var mock = new Mock<IGenericRepository<Mastery>>();
//            mock.Setup(m => m.Delete(It.IsAny<Mastery>()));

//            var target = new MasteriesController(mock.Object);

//            // act
//            var result = target.DeleteConfirmed(1) as RedirectToActionResult;

//            // assert
//            Assert.NotNull(result);
//            Assert.Equal("Index", result.ActionName);
//        }
//    }
//}
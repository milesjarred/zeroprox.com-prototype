# Base image
FROM microsoft/aspnet:1.0.0-rc1-update1

# Env Variables
# These are left blank so values are not committed to repository
ENV ASPNET_ENV="Staging" App:reCaptcha:Secret="" App:reCaptcha:SiteKey="" App:Email:SmtpServer=""
ENV App:Email:Username="" App:Email:Password="" Auth:Facebook:AppId="" Auth:Facebook:Secret="" Auth:Twitter:AppKey=""
ENV Auth:Twitter:Secret="" Data:ConnectionString=""

# Copy projects into the app folder
ADD ["src/Zeroprox.Domain", "/app/Zeroprox.Domain"]
ADD ["src/Zeroprox.Web", "/app/Zeroprox.Web/"]

# Restore packages
WORKDIR /app/Zeroprox.Domain
RUN ["dnu", "restore"]

WORKDIR /app/Zeroprox.Web
RUN ["dnu", "restore"]

# Volume for static files
VOLUME ["/app/Zeroprox.Web/wwwroot/uploads"]

# Start application
ENTRYPOINT ["dnx","-p","project.json", "web"]
using System.Collections.Generic;
using Microsoft.AspNet.Mvc;
using Zeroprox.Domain.Abstract.IRepositories;
using Zeroprox.Domain.Entities;

namespace Zeroprox.Web.Api
{
    [Produces("application/json")]
    [Route("api/v1/WorkExperiences")]
    public class WorkExperiencesApiController : Controller
    {
        private readonly IGenericRepository<WorkExperience> _workRepository;

        public WorkExperiencesApiController(IGenericRepository<WorkExperience> workRepository)
        {
            _workRepository = workRepository;
        }

        // GET: api/WorkExperiencesApi
        [HttpGet]
        public IEnumerable<WorkExperience> GetWorksExperiences()
        {
            return _workRepository.GetAll();
        }

        // GET: api/WorkExperiencesApi/5
        [HttpGet("{id}", Name = "GetWorkExperience")]
        public IActionResult GetWorkExperience([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            var workExperience = _workRepository.GetById(id);

            if (workExperience == null)
            {
                return HttpNotFound();
            }

            return Ok(workExperience);
        }
    }
}
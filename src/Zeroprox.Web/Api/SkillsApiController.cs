using System.Collections.Generic;
using Microsoft.AspNet.Mvc;
using Zeroprox.Domain.Abstract.IUnitOfWork;
using Zeroprox.Domain.Entities;

namespace Zeroprox.Web.Api
{
    [Produces("application/json")]
    [Route("api/v1/Skills")]
    public class SkillsApiController : Controller
    {
        private readonly ISkillsUnitOfWork _skillsUnitOfWork;

        public SkillsApiController(ISkillsUnitOfWork skillsUnitOfWork)
        {
            _skillsUnitOfWork = skillsUnitOfWork;
        }

        // GET: api/SkillsApi
        [HttpGet]
        public IEnumerable<Skill> GetSkills()
        {
            return _skillsUnitOfWork.SkillRepository.GetSkillsWithMasteries();
        }

        // GET: api/SkillsApi/5
        [HttpGet("{id}", Name = "GetSkill")]
        public IActionResult GetSkill([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            var skill = _skillsUnitOfWork.SkillRepository.GetById(id);

            if (skill == null)
            {
                return HttpNotFound();
            }

            return Ok(skill);
        }
    }
}
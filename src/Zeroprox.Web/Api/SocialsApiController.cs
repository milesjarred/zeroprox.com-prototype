using System.Collections.Generic;
using Microsoft.AspNet.Mvc;
using Zeroprox.Domain.Abstract.IUnitOfWork;
using Zeroprox.Domain.Entities;

namespace Zeroprox.Web.Api
{
    [Produces("application/json")]
    [Route("api/v1/Socials")]
    public class SocialsApiController : Controller
    {
        private readonly ISocialUnitOfWork _socialUnitOfWork;

        public SocialsApiController(ISocialUnitOfWork socialUnitOfWork)
        {
            _socialUnitOfWork = socialUnitOfWork;
        }

        // GET: api/Socials
        [HttpGet]
        public IEnumerable<Social> GetSocials()
        {
            return _socialUnitOfWork.SocialRepository.GetSocialWithFile();
        }

        // GET: api/Socials/5
        [HttpGet("{id}", Name = "GetSocial")]
        public IActionResult GetSocial([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            var social = _socialUnitOfWork.SocialRepository.GetById(id);

            if (social == null)
            {
                return HttpNotFound();
            }

            return Ok(social);
        }
    }
}
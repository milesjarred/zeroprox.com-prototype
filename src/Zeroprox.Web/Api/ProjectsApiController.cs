using System.Collections.Generic;
using Microsoft.AspNet.Mvc;
using Zeroprox.Domain.Abstract.IUnitOfWork;
using Zeroprox.Domain.Entities;

namespace Zeroprox.Web.Api
{
    [Produces("application/json")]
    [Route("api/v1/Projects")]
    public class ProjectsApiController : Controller
    {
        private readonly IProjectUnitOfWork _projectUnitOfWork;

        public ProjectsApiController(IProjectUnitOfWork projectUnitOfWork)
        {
            _projectUnitOfWork = projectUnitOfWork;
        }

        // GET: api/ProjectsApi
        [HttpGet]
        public IEnumerable<Project> GetProjects()
        {
            return _projectUnitOfWork.ProjectRepository.GetProjectsWithFiles();
        }

        // GET: api/ProjectsApi/5
        [HttpGet("{id}", Name = "GetProject")]
        public IActionResult GetProject([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            var project = _projectUnitOfWork.ProjectRepository.GetById(id);

            if (project == null)
            {
                return HttpNotFound();
            }

            return Ok(project);
        }
    }
}
using System.Collections.Generic;
using Microsoft.AspNet.Mvc;
using Zeroprox.Domain.Abstract.IRepositories;
using Zeroprox.Domain.Entities;

namespace Zeroprox.Web.Api
{
    [Produces("application/json")]
    [Route("api/v1/Educations")]
    public class EducationsApiController : Controller
    {
        private readonly IGenericRepository<Education> _educationRepository;

        public EducationsApiController(IGenericRepository<Education> educationRepository)
        {
            _educationRepository = educationRepository;
        }

        // GET: api/EducationsApi
        [HttpGet]
        public IEnumerable<Education> GetEducations()
        {
            return _educationRepository.GetAll();
        }

        // GET: api/EducationsApi/5
        [HttpGet("{id}", Name = "GetEducation")]
        public IActionResult GetEducation([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            var education = _educationRepository.GetById(id);

            if (education == null)
            {
                return HttpNotFound();
            }

            return Ok(education);
        }
    }
}
﻿using System.Linq;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using Microsoft.Extensions.OptionsModel;
using MimeKit;
using Zeroprox.Domain.Entities.Options;

namespace Zeroprox.Web.Services.Email
{
    public class EmailService : IEmailService
    {
        public void SendEmail(string email, string sendTo, string subject, string plainMsg,
            IOptions<EmailOptions> options, string htmlMsg = null)
        {
            // Plug in your email service here to send an email.

            var mail = new MimeMessage();
            mail.From.Add(new MailboxAddress("Zeroprox.com", "no-reply@zeroprox.com"));
            mail.To.Add(new MailboxAddress(sendTo, email));
            mail.Subject = subject;

            var builder = new BodyBuilder
            {
                // Plain text version of the email
                TextBody = plainMsg,
                // Html version of the email
                HtmlBody = htmlMsg
            };

            mail.Body = builder.ToMessageBody();

            using (var client = new SmtpClient())
            {
                client.Connect(options.Value.SmtpServer, 587, false);

                // Note: since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                client.AuthenticationMechanisms.Remove("XOAUTH2");

                // Note: only needed if the SMTP server requires authentication
                client.Authenticate(options.Value.Username, options.Value.Password);

                client.Send(mail);
                client.Disconnect(true);
            }
        }
    }
}
﻿using Microsoft.Extensions.OptionsModel;
using Zeroprox.Domain.Entities.Options;

namespace Zeroprox.Web.Services.Email
{
    public interface IEmailService
    {
        void SendEmail(string email, string sendTo, string subject,
            string plainMsg, IOptions<EmailOptions> options, string htmlMsg = null);
    }
}
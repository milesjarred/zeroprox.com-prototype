﻿// Controller for the work experiences section

app.controller('workCtrl', function ($scope, appServices) {
    // Get the all work experiences from the appService
    appServices.getWorkExperiences(function (response) {
        $scope.works = response.data;
    });
});
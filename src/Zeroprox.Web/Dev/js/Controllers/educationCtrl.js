﻿// Controller for the education section

app.controller('educationCtrl', function ($scope, appServices) {
    // Get the all educations from the appService
    appServices.getEducations(function(response) {
        $scope.educations = response.data;
    });
});

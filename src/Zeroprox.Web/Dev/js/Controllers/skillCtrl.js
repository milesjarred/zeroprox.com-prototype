﻿// Controller for the skills section

app.controller('skillCtrl', function ($scope, appServices) {
    // Get the all skills from the appService
    appServices.getSkills(function (response) {
        $scope.skills = response.data;
    });
});
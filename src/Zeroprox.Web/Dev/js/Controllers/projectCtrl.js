﻿// Controller for the projects section

app.controller('projectCtrl', function ($scope, appServices) {
    // Get the all projects from the appService
    appServices.getProjects(function (response) {
        $scope.projects = response.data;
    });
});
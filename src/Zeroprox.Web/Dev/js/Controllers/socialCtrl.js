﻿// Controller for the about and header section

app.controller('socialCtrl', function ($scope, appServices) {
    // Get the all socials from the appService
    appServices.getSocials(function (response) {
        $scope.socials = response.data;
    });
});
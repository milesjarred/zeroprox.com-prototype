﻿app.service('appServices', function($http) {
    // Get educations
    this.getEducations = function(callback) {
        $http.get('api/v1/Educations')
            .then(callback);
    };
    // Get projects
    this.getProjects = function(callback) {
        $http.get('api/v1/Projects')
            .then(callback);
    };
    // Get skills
    this.getSkills = function(callback) {
        $http.get('api/v1/Skills')
            .then(callback);
    };
    // Get work experiences
    this.getWorkExperiences = function(callback) {
        $http.get('api/v1/WorkExperiences')
            .then(callback);
    };
    // Get socials
    this.getSocials = function(callback) {
        $http.get('api/v1/Socials/')
            .then(callback);
    };
});
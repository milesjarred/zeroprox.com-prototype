namespace Zeroprox.Web.Constants
{
    public static class CacheProfileName
    {
        public const string BrowserConfigXml = "BrowserConfigXml";
        public const string Error = "Error";
        public const string ManifestJson = "ManifestJson";
        public const string RobotsText = "RobotsText";
        public const string SitemapNodes = "SitemapNodes";
    }
}
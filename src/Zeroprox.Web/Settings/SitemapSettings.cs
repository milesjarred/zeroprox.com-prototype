﻿namespace Zeroprox.Web.Settings
{
    public class SitemapSettings
    {
        public string[] SitemapPingLocations { get; set; }
    }
}
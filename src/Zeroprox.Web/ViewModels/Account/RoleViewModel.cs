﻿using System.ComponentModel.DataAnnotations;


namespace Zeroprox.Web.ViewModels.Account
{
    public class RoleViewModel
    {
        [Required]
        public string Name { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Mvc.Rendering;

namespace Zeroprox.Web.ViewModels.Account
{
    public class UserRolesViewModel
    {
        [Display(Name = "Username:")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Email:")]
        public string Email { get; set; }

        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }

        // Used for displaying roles
        [Display(Name = "Roles:")]
        public IEnumerable<string> Roles { get; set; }
        // Used for assigning roles
        [Display(Name = "Roles:")]
        public IEnumerable<SelectListItem> SelectedRole { get; set; }
    }
}

using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Extensions.DependencyInjection;
using Zeroprox.Web.Services;
using Zeroprox.Domain.Abstract.IRepositories;
using Zeroprox.Domain.Abstract.IUnitOfWork;
using Zeroprox.Domain.EF.Repositories;
using Zeroprox.Domain.EF.UnitOfWorks;
using Zeroprox.Domain.Entities;
using Zeroprox.Domain.Entities.Files;
using Zeroprox.Web.Services.Email;

namespace Zeroprox.Web
{
    public partial class Startup
    {
        /// <summary>
        ///     Configures custom services to add to the ASP.NET MVC 6 Injection of Control (IoC) container.
        /// </summary>
        /// <param name="services">The services collection or IoC container.</param>
        private static void ConfigureCustomServices(IServiceCollection services)
        {
            services.AddScoped<IBrowserConfigService, BrowserConfigService>();
            services.AddScoped<IManifestService, ManifestService>();
            services.AddScoped<IRobotsService, RobotsService>();
            services.AddScoped<ISitemapService, SitemapService>();
            services.AddScoped<ISitemapPingerService, SitemapPingerService>();

            // Add your own custom services here e.g.
            services.AddTransient<IEmailService, EmailService>();

            // DI
            services.AddTransient<IGenericRepository<Mastery>, GenericRepository<Mastery>>();
            services.AddTransient<IGenericRepository<FileUpload>, GenericRepository<FileUpload>>();
            services.AddTransient<IGenericRepository<Education>, GenericRepository<Education>>();
            services.AddTransient<IGenericRepository<WorkExperience>, GenericRepository<WorkExperience>>();

            services.AddTransient<ISkillsUnitOfWork, SkillsUnitOfWork>();
            services.AddTransient<IProjectUnitOfWork, ProjectUnitOfWork>();
            services.AddTransient<ISocialUnitOfWork, SocialUnitOfWork>();

            // Singleton - Only one instance is ever created and returned.
            // services.AddSingleton<IExampleService, ExampleService>();

            // Scoped - A new instance is created and returned for each request/response cycle.
            // services.AddScoped<IExampleService, ExampleService>();

            // Transient - A new instance is created and returned each time.
            // services.AddTransient<IExampleService, ExampleService>();
        }
    }
}
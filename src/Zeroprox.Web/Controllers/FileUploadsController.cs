using System;
using System.IO;
using System.Linq;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Net.Http.Headers;
using Zeroprox.Domain.Abstract.IRepositories;
using Zeroprox.Domain.Entities.Files;

namespace Zeroprox.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("Admin/[controller]")]
    public class FileUploadsController : Controller
    {
        private readonly IHostingEnvironment _environment;
        private readonly IGenericRepository<FileUpload> _fileRepository;

        public FileUploadsController(IGenericRepository<FileUpload> fileRepository,
            IHostingEnvironment environment)
        {
            _fileRepository = fileRepository;
            _environment = environment;
        }

        // GET: Files
        public IActionResult Index()
        {
            return View(_fileRepository.GetAll());
        }

        // GET: Files/Details/5
        [HttpGet("Details/{id:int?}")]
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var file = _fileRepository.GetById(id);
            if (file == null)
            {
                return HttpNotFound();
            }
            return View(file);
        }

        // GET: Files/Create
        [HttpGet("Create")]
        public IActionResult Create()
        {
            PopulateFileTypeDropDownList();
            return View("Create");
        }

        // POST: Files/Create
        [HttpPost("Create")]
        [ValidateAntiForgeryToken]
        public IActionResult Create(FileUpload file, IFormFile uploadedFile)
        {
            if (ModelState.IsValid)
            {
                // uploads a file to the server
                // and saves file info in db
                UploadFile(file, uploadedFile);

                _fileRepository.Create(file);

                return RedirectToAction("Index");
            }
            return View(file);
        }

        // ToDo: Implment ability to upload a new file upon editing?
        // GET: Files/Edit/5
        [HttpGet("Edit/{id:int?}")]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var file = _fileRepository.GetById(id);
            if (file == null)
            {
                return HttpNotFound();
            }
            PopulateFileTypeDropDownList(file.FileType);
            return View(file);
        }

        // POST: Files/Edit/5
        [HttpPost("Edit")]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(FileUpload file)
        {
            if (ModelState.IsValid)
            {
                _fileRepository.Update(file);
                return RedirectToAction("Index");
            }
            return View(file);
        }

        // GET: Files/Delete/5
        [HttpGet("Delete/{id:int?}"), ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var file = _fileRepository.GetById(id);
            if (file == null)
            {
                return HttpNotFound();
            }
            return View(file);
        }

        // POST: Files/Delete/5
        [HttpPost("Delete/{id:int}"), ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var file = _fileRepository.GetById(id);

            // Delete the file in uploads folder
            DeleteUploadedFile(_environment.WebRootPath + file.FilePath);

            // Delete file from db
            _fileRepository.Delete(file);
            return RedirectToAction("Index");
        }

        private void PopulateFileTypeDropDownList(object selectedFile = null)
        {
            var fileTypes = Enum.GetValues(typeof (FileType)).Cast<FileType>();

            ViewData["FileTypes"] = new SelectList(fileTypes, selectedFile);
        }

        private void UploadFile(FileUpload file, IFormFile uploadedFile)
        {
            var uploadsFolderPath = Path.Combine(_environment.WebRootPath, "uploads");

            if (uploadedFile.Length > 0)
            {
                // get the file name
                var fileName = ContentDispositionHeaderValue.Parse(uploadedFile.ContentDisposition).FileName.Trim('"');

                // get the extension of the file
                var fileExtension = Path.GetExtension(fileName);


                // Set the file type depending on the extension
                switch (fileExtension)
                {
                    case ".jpg":
                    case ".jpeg":
                    case ".png":
                    case ".gif":
                    case ".svg":
                        file.FileType = FileType.Image;
                        break;
                    case ".doc":
                    case ".docx":
                        file.FileType = FileType.WordDoc;
                        break;
                    case ".pdf":
                        file.FileType = FileType.Pdf;
                        break;
                    default:
                        file.FileType = FileType.Other;
                        break;
                }

                // create unique name for the database
                var dbFileName = Guid.NewGuid() + fileExtension;

                // path to save file to
                var filePath = Path.Combine(uploadsFolderPath, dbFileName);

                // save the file to the uploads folder
                uploadedFile.SaveAs(filePath);

                // set file path to entite.FilePath
                file.FilePath = "\\uploads\\" + dbFileName;
            }
        }

        private void DeleteUploadedFile(string filePath)
        {
            // Check if file exists
            if (System.IO.File.Exists(filePath))
            {
                // Delete the file
                System.IO.File.Delete(filePath);
            }
        }
    }
}
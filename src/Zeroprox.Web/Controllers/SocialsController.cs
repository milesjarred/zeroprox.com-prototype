using System.Linq;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Zeroprox.Domain.Abstract.IUnitOfWork;
using Zeroprox.Domain.Entities;

namespace Zeroprox.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("Admin/[controller]")]
    public class SocialsController : Controller
    {
        private readonly ISocialUnitOfWork _socialUnitOfWork;

        public SocialsController(ISocialUnitOfWork socialUnitOfWork)
        {
            _socialUnitOfWork = socialUnitOfWork;
        }

        // GET: social
        public IActionResult Index()
        {
            return View(_socialUnitOfWork.SocialRepository.GetSocialWithFile());
        }

        // GET: social/Details/5
        [HttpGet("Details/{id:int?}")]
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var social = _socialUnitOfWork.SocialRepository.GetById(id);
            if (social == null)
            {
                return HttpNotFound();
            }

            return View(social);
        }

        // GET: social/Create
        [HttpGet("Create")]
        public IActionResult Create()
        {
            var getCount = _socialUnitOfWork.SocialRepository.GetSocialWithFile().Count();
            if (getCount >= 1)
            {
                return RedirectToAction("Index");
            }
            PopulateFileDropDownList();
            return View();
        }

        // POST: social/Create
        [HttpPost("Create")]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Social social)
        {
            if (ModelState.IsValid)
            {
                _socialUnitOfWork.SocialRepository.Create(social);
                return RedirectToAction("Index");
            }
            PopulateFileDropDownList(social.FileId);
            return View(social);
        }

        // GET: social/Edit/5
        [HttpGet("Edit/{id:int?}")]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var social = _socialUnitOfWork.SocialRepository.GetById(id);
            if (social == null)
            {
                return HttpNotFound();
            }
            PopulateFileDropDownList(social.FileId);
            return View(social);
        }

        // POST: social/Edit/5
        [HttpPost("Edit")]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Social social)
        {
            if (ModelState.IsValid)
            {
                _socialUnitOfWork.SocialRepository.Update(social);
                return RedirectToAction("Index");
            }
            PopulateFileDropDownList(social.FileId);
            return View(social);
        }

        // GET: social/Delete/5
        [HttpGet("Delete/{id:int?}"), ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var social = _socialUnitOfWork.SocialRepository.GetById(id);
            if (social == null)
            {
                return HttpNotFound();
            }

            return View(social);
        }

        // POST: social/Delete/5
        [HttpPost("Delete/{id:int}"), ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var social = _socialUnitOfWork.SocialRepository.GetById(id);
            _socialUnitOfWork.SocialRepository.Delete(social);
            return RedirectToAction("Index");
        }

        private void PopulateFileDropDownList(object selectedFile = null)
        {
            var fileQuery = _socialUnitOfWork.FileRepository
                .GetAll().OrderBy(m => m.Name);

            ViewData["FileId"] = new SelectList(fileQuery, "FileId", "Name", selectedFile);
        }

        protected override void Dispose(bool disposing)
        {
            _socialUnitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Zeroprox.Domain.EF;
using Zeroprox.Domain.Entities;
using Zeroprox.Web.ViewModels.Account;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Zeroprox.Web.Controllers.Roles
{
    [Route("Admin/Users")]
    public class UserRolesController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public UserRolesController(UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        // GET: /Users
        public IActionResult Index()
        {
            return View(_userManager.Users);
        }

        // GET: Users/Details/Zeroprox
        [HttpGet("Details/{username}")]
        public async Task<IActionResult> Details(string username)
        {
            if (username == null)
            {
                return HttpNotFound();
            }

            var user = await _userManager.FindByNameAsync(username);

            if (user == null)
            {
                return HttpNotFound();
            }

            var userVm = new UserRolesViewModel
            {
                UserName = user.UserName,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Roles = await _userManager.GetRolesAsync(user)
            };

            return View(userVm);
        }

        // GET: Users/Edit/zeroprox
        [HttpGet("Roles/{username}")]
        public async Task<IActionResult> EditRoles(string username)
        {
            if (username == null)
            {
                return HttpNotFound();
            }
            var user = await _userManager.FindByNameAsync(username);
            if (user == null)
            {
                return HttpNotFound();
            }

            var userRoles = await _userManager.GetRolesAsync(user);

            var userVm = new UserRolesViewModel
            {
                UserName = user.UserName,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                SelectedRole = _roleManager.Roles.ToList().Select(x => new SelectListItem()
                {
                    Selected = userRoles.Contains(x.Name),
                    Text = x.Name,
                    Value = x.Name
                })
            };

            return View(userVm);
        }

        // POST: Users/Edit/zeroprox
        [HttpPost("Roles/{username}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditRoles(UserRolesViewModel userVm, params string[] selectedRole)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(userVm.UserName);

                if (user == null)
                {
                    return HttpNotFound();
                }

                user.UserName = userVm.UserName;
                user.Email = userVm.Email;
                user.FirstName = userVm.FirstName;
                user.LastName = userVm.LastName;

                // Get a list of possible roles for the user and 
                // add them to the array when selected
                var userRoles = await _userManager.GetRolesAsync(user);

                selectedRole = selectedRole ?? new string[] { };

                // Add a user to the selected roles
                var result = await _userManager.AddToRolesAsync(user,
                    selectedRole.Except(userRoles).ToArray());

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First().ToString());
                    return View();
                }

                // Remove from role if unselected checkbox
                result = await _userManager.RemoveFromRolesAsync(user,
                    userRoles.Except(selectedRole).ToArray());

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First().ToString());
                    return View();
                }
                return RedirectToAction("Index");
            }
            return View(userVm);
        }

        // GET: Users/Delete/zeroprox
        [HttpGet("Delete/{username}"), ActionName("Delete")]
        public async Task<IActionResult> Delete(string username)
        {
            if (username == null)
            {
                return HttpNotFound();
            }

            var user = await _userManager.FindByNameAsync(username);
            
            if (user == null)
            {
                return HttpNotFound();
            }

            if (user.NormalizedUserName == "ZEROPROX")
            {
                return RedirectToAction("Index");
            }

            return View(user);
        }

        // POST: Users/Delete/zeroprox
        [HttpPost("Delete/{username}"), ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string username)
        {
            var user = await _userManager.FindByNameAsync(username);

            if (user.NormalizedUserName == "ZEROPROX")
            {
                return RedirectToAction("Index");
            }

            await _userManager.DeleteAsync(user);
            return RedirectToAction("Index");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Zeroprox.Domain.Abstract.IRepositories;
using Zeroprox.Web.ViewModels.Account;
using Zeroprox.Domain.EF;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Zeroprox.Web.Controllers.Roles
{
    [Authorize(Roles = "Admin")]
    [Route("Admin/Roles")]
    public class RolesController : Controller
    {
        private readonly RoleManager<IdentityRole> _roleManager;  

        public RolesController(RoleManager<IdentityRole> roleManager)
        {
            _roleManager = roleManager;
        }
         
        // GET: Roles/Index
        public IActionResult Index()
        {
            return View(_roleManager.Roles);
        }

        // GET: Roles/Create
        [HttpGet("Create")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Roles/Create
        [HttpPost("Create")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(RoleViewModel userRole)
        {
            if (ModelState.IsValid)
            {
                var role = new IdentityRole(userRole.Name);

                await _roleManager.CreateAsync(role);
                return RedirectToAction("Index");
            }
            return View(userRole);
        }

        // GET: Roles/Delete/Mod
        [HttpGet("Delete/{name}"), ActionName("Delete")]
        public async Task<IActionResult> Delete(string name)
        {
            if (name == null)
            {
                return HttpNotFound();
            }

            var role = await _roleManager.FindByNameAsync(name);

            if (role == null)
            {
                return HttpNotFound();
            }

            if (role.NormalizedName == "ADMIN")
            {
                return RedirectToAction("Index");
            }

            return View(role);
        }

        // POST: Roles/Delete/Mod
        [HttpPost("Delete/{name}"), ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string name)
        {
            var role = await _roleManager.FindByNameAsync(name);

            if (role.NormalizedName == "ADMIN")
            {
                return RedirectToAction("Index");
            }

            await _roleManager.DeleteAsync(role);
            return RedirectToAction("Index");
        }
    }
}

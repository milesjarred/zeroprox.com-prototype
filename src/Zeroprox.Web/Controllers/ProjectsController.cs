using System.Linq;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Zeroprox.Domain.Abstract.IUnitOfWork;
using Zeroprox.Domain.Entities;

namespace Zeroprox.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("Admin/[controller]")]
    public class ProjectsController : Controller
    {
        private readonly IProjectUnitOfWork _projectUnitOfWork;

        public ProjectsController(IProjectUnitOfWork projectUnitOfWork)
        {
            _projectUnitOfWork = projectUnitOfWork;
        }

        // GET: Project
        public IActionResult Index()
        {
            return View(_projectUnitOfWork.ProjectRepository.GetProjectsWithFiles());
        }

        // GET: Project/Details/5
        [HttpGet("Details/{id:int?}")]
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var project = _projectUnitOfWork.ProjectRepository.GetById(id);
            var file = _projectUnitOfWork.FilesRepository.GetById(project.FileId);

            ViewData["FileName"] = file.Name;

            if (project == null)
            {
                return HttpNotFound();
            }

            return View(project);
        }

        // GET: Project/Create
        [HttpGet("Create")]
        public IActionResult Create()
        {
            PopulateFileDropDownList();
            return View();
        }

        // POST: Project/Create
        [HttpPost("Create")]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Project project)
        {
            if (ModelState.IsValid)
            {
                _projectUnitOfWork.ProjectRepository.Create(project);
                return RedirectToAction("Index");
            }
            PopulateFileDropDownList(project.FileId);
            return View(project);
        }

        // GET: Project/Edit/5
        [HttpGet("Edit/{id:int?}")]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var project = _projectUnitOfWork.ProjectRepository.GetById(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            PopulateFileDropDownList(project.FileId);
            return View(project);
        }

        // POST: Project/Edit/5
        [HttpPost("Edit")]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Project project)
        {
            if (ModelState.IsValid)
            {
                _projectUnitOfWork.ProjectRepository.Update(project);
                return RedirectToAction("Index");
            }
            PopulateFileDropDownList(project.FileId);
            return View(project);
        }

        // GET: Project/Delete/5
        [HttpGet("Delete/{id:int?}"), ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var project = _projectUnitOfWork.ProjectRepository.GetById(id);
            if (project == null)
            {
                return HttpNotFound();
            }

            return View(project);
        }

        // POST: Project/Delete/5
        [HttpPost("Delete/{id:int}"), ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var project = _projectUnitOfWork.ProjectRepository.GetById(id);
            _projectUnitOfWork.ProjectRepository.Delete(project);
            return RedirectToAction("Index");
        }

        private void PopulateFileDropDownList(object selectedFile = null)
        {
            var fileQuery = _projectUnitOfWork.FilesRepository
                .GetAll().OrderBy(m => m.Name);

            ViewData["FileId"] = new SelectList(fileQuery, "FileId", "Name", selectedFile);
        }

        protected override void Dispose(bool disposing)
        {
            _projectUnitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
using System.Text;
using System.Threading.Tasks;
using Boilerplate.Web.Mvc;
using Boilerplate.Web.Mvc.Filters;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.OptionsModel;
using Zeroprox.Web.Constants;
using Zeroprox.Web.Services;
using Zeroprox.Web.Settings;

namespace Zeroprox.Web.Controllers.Home
{
    public class HomeController : Controller
    {
        #region Constructors

        public HomeController(
            IBrowserConfigService browserConfigService,
            IManifestService manifestService,
            IRobotsService robotsService,
            ISitemapService sitemapService,
            IOptions<AppSettings> appSettings)
        {
            this.appSettings = appSettings;
            this.browserConfigService = browserConfigService;
            this.manifestService = manifestService;
            this.robotsService = robotsService;
            this.sitemapService = sitemapService;
        }

        #endregion

        [HttpGet("", Name = HomeControllerRoute.GetIndex)]
        public IActionResult Index()
        {
            return View(HomeControllerAction.Index);
        }

        /// <summary>
        ///     Gets the browserconfig XML for the current site. This allows you to customize the tile, when a user pins
        ///     the site to their Windows 8/10 start screen. See http://www.buildmypinnedsite.com and
        ///     https://msdn.microsoft.com/en-us/library/dn320426%28v=vs.85%29.aspx
        /// </summary>
        /// <returns>The browserconfig XML for the current site.</returns>
        [NoTrailingSlash]
        [ResponseCache(CacheProfileName = CacheProfileName.BrowserConfigXml)]
        [Route("browserconfig.xml", Name = HomeControllerRoute.GetBrowserConfigXml)]
        public ContentResult BrowserConfigXml()
        {
            var content = browserConfigService.GetBrowserConfigXml();
            return Content(content, ContentType.Xml, Encoding.UTF8);
        }

        /// <summary>
        ///     Gets the manifest JSON for the current site. This allows you to customize the icon and other browser
        ///     settings for Chrome/Android and FireFox (FireFox support is coming). See https://w3c.github.io/manifest/
        ///     for the official W3C specification. See http://html5doctor.com/web-manifest-specification/ for more
        ///     information. See https://developer.chrome.com/multidevice/android/installtohomescreen for Chrome's
        ///     implementation.
        /// </summary>
        /// <returns>The manifest JSON for the current site.</returns>
        [NoTrailingSlash]
        [ResponseCache(CacheProfileName = CacheProfileName.ManifestJson)]
        [Route("manifest.json", Name = HomeControllerRoute.GetManifestJson)]
        public ContentResult ManifestJson()
        {
            var content = manifestService.GetManifestJson();
            return Content(content, ContentType.Json, Encoding.UTF8);
        }

        /// <summary>
        ///     Tells search engines (or robots) how to index your site.
        ///     The reason for dynamically generating this code is to enable generation of the full absolute sitemap URL
        ///     and also to give you added flexibility in case you want to disallow search engines from certain paths. The
        ///     sitemap is cached for one day, adjust this time to whatever you require. See
        ///     http://rehansaeed.com/dynamically-generating-robots-txt-using-asp-net-mvc/
        /// </summary>
        /// <returns>The robots text for the current site.</returns>
        [NoTrailingSlash]
        [ResponseCache(CacheProfileName = CacheProfileName.RobotsText)]
        [Route("robots.txt", Name = HomeControllerRoute.GetRobotsText)]
        public IActionResult RobotsText()
        {
            var content = robotsService.GetRobotsText();
            return Content(content, ContentType.Text, Encoding.UTF8);
        }

        /// <summary>
        ///     Gets the sitemap XML for the current site. You can customize the contents of this XML from the
        ///     <see cref="SitemapService" />. The sitemap is cached for one day, adjust this time to whatever you require.
        ///     http://www.sitemaps.org/protocol.html
        /// </summary>
        /// <param name="index">
        ///     The index of the sitemap to retrieve. <c>null</c> if you want to retrieve the root
        ///     sitemap file, which may be a sitemap index file.
        /// </param>
        /// <returns>The sitemap XML for the current site.</returns>
        [NoTrailingSlash]
        [Route("sitemap.xml", Name = HomeControllerRoute.GetSitemapXml)]
        public async Task<IActionResult> SitemapXml(int? index = null)
        {
            var content = await sitemapService.GetSitemapXml(index);

            if (content == null)
            {
                return HttpBadRequest("Sitemap index is out of range.");
            }

            return Content(content, ContentType.Xml, Encoding.UTF8);
        }

        #region Fields

        private readonly IOptions<AppSettings> appSettings;
        private readonly IBrowserConfigService browserConfigService;
        private readonly IManifestService manifestService;
        private readonly IRobotsService robotsService;
        private readonly ISitemapService sitemapService;

        #endregion
    }
}
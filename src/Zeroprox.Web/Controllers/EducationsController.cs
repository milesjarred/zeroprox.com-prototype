using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using Zeroprox.Domain.Abstract.IRepositories;
using Zeroprox.Domain.Entities;

namespace Zeroprox.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("Admin/[controller]")]
    public class EducationsController : Controller
    {
        private readonly IGenericRepository<Education> _educationRepository;

        public EducationsController(IGenericRepository<Education> educationRepository)
        {
            _educationRepository = educationRepository;
        }

        // GET: Educations
        public IActionResult Index()
        {
            return View(_educationRepository.GetAll());
        }

        // GET: Educations/Details/5
        [HttpGet("Details/{id:int?}")]
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var education = _educationRepository.GetById(id);
            if (education == null)
            {
                return HttpNotFound();
            }

            return View(education);
        }

        // GET: Educations/Create
        [HttpGet("Create")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Educations/Create
        [HttpPost("Create")]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Education education)
        {
            if (ModelState.IsValid)
            {
                _educationRepository.Create(education);
                return RedirectToAction("Index");
            }
            return View(education);
        }

        // GET: Educations/Edit/5
        [HttpGet("Edit/{id:int?}")]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var education = _educationRepository.GetById(id);
            if (education == null)
            {
                return HttpNotFound();
            }
            return View(education);
        }

        // POST: Educations/Edit/5
        [HttpPost("Edit")]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Education education)
        {
            if (ModelState.IsValid)
            {
                _educationRepository.Update(education);
                return RedirectToAction("Index");
            }
            return View(education);
        }

        // GET: Educations/Delete/5
        [HttpGet("Delete/{id:int?}"), ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var education = _educationRepository.GetById(id);
            if (education == null)
            {
                return HttpNotFound();
            }

            return View(education);
        }

        // POST: Educations/Delete/5
        [HttpPost("Delete/{id:int}"), ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var education = _educationRepository.GetById(id);
            _educationRepository.Delete(education);
            return RedirectToAction("Index");
        }
    }
}
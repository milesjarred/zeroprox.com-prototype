using System;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using Zeroprox.Domain.Abstract.IRepositories;
using Zeroprox.Domain.Entities;

namespace Zeroprox.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("Admin/[controller]")]
    public class WorkExperiencesController : Controller
    {
        private readonly IGenericRepository<WorkExperience> _workRepository;

        public WorkExperiencesController(IGenericRepository<WorkExperience> workRepository)
        {
            _workRepository = workRepository;
        }

        // GET: WorkExperiences
        public IActionResult Index()
        {
            return View(_workRepository.GetAll());
        }

        // GET: WorkExperiences/Details/5
        [HttpGet("Details/{id:int?}")]
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var workExperience = _workRepository.GetById(id);
            if (workExperience == null)
            {
                return HttpNotFound();
            }

            return View(workExperience);
        }

        // GET: WorkExperiences/Create
        [HttpGet("Create")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: WorkExperiences/Create
        [HttpPost("Create")]
        [ValidateAntiForgeryToken]
        public IActionResult Create(WorkExperience workExperience)
        {
            if (ModelState.IsValid)
            {
                _workRepository.Create(workExperience);
                return RedirectToAction("Index");
            }
            return View(workExperience);
        }

        // GET: WorkExperiences/Edit/5
        [HttpGet("Edit/{id:int?}")]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var workExperience = _workRepository.GetById(id);
            if (workExperience == null)
            {
                return HttpNotFound();
            }
            return View(workExperience);
        }

        // POST: WorkExperiences/Edit/5
        [HttpPost("Edit")]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(WorkExperience workExperience)
        {
            if (ModelState.IsValid)
            {
                _workRepository.Update(workExperience);
                return RedirectToAction("Index");
            }
            return View(workExperience);
        }

        // GET: WorkExperiences/Delete/5
        [HttpGet("Delete/{id:int?}"), ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var workExperience = _workRepository.GetById(id);
            if (workExperience == null)
            {
                return HttpNotFound();
            }

            return View(workExperience);
        }

        // POST: WorkExperiences/Delete/5
        [HttpPost("Delete/{id:int}"), ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var workExperience = _workRepository.GetById(id);
            _workRepository.Delete(workExperience);
            return RedirectToAction("Index");
        }
    }
}
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using Zeroprox.Domain.Abstract.IRepositories;
using Zeroprox.Domain.Entities;

namespace Zeroprox.Web.Controllers.Skills
{
    [Authorize(Roles = "Admin")]
    [Route("Admin/[controller]")]
    public class MasteriesController : Controller
    {
        private readonly IGenericRepository<Mastery> _masteryRepository;

        public MasteriesController(IGenericRepository<Mastery> masteryRepository)
        {
            _masteryRepository = masteryRepository;
        }

        // GET: Mastery
        public IActionResult Index()
        {
            return View(_masteryRepository.GetAll());
        }

        // GET: Mastery/Details/5
        [HttpGet("Details/{id:int?}")]
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var mastery = _masteryRepository.GetById(id);
            if (mastery == null)
            {
                return HttpNotFound();
            }

            return View(mastery);
        }

        // GET: Mastery/Create
        [HttpGet("Create")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Mastery/Create
        [HttpPost("Create")]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Mastery mastery)
        {
            if (ModelState.IsValid)
            {
                _masteryRepository.Create(mastery);
                return RedirectToAction("Index");
            }
            return View("Create", mastery);
        }

        // GET: Mastery/Edit/5
        [HttpGet("Edit/{id:int?}")]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var mastery = _masteryRepository.GetById(id);
            if (mastery == null)
            {
                return HttpNotFound();
            }
            return View(mastery);
        }

        // POST: Mastery/Edit/5
        [HttpPost("Edit")]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Mastery mastery)
        {
            if (ModelState.IsValid)
            {
                _masteryRepository.Update(mastery);
                return RedirectToAction("Index");
            }
            return View(mastery);
        }

        // GET: Mastery/Delete/5
        [HttpGet("Delete/{id:int?}"), ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var mastery = _masteryRepository.GetById(id);
            if (mastery == null)
            {
                return HttpNotFound();
            }

            return View(mastery);
        }

        // POST: Mastery/Delete/5
        [HttpPost("Delete/{id:int}"), ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var mastery = _masteryRepository.GetById(id);
            _masteryRepository.Delete(mastery);
            return RedirectToAction("Index");
        }
    }
}
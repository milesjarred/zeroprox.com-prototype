using System.Linq;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Zeroprox.Domain.Abstract.IUnitOfWork;
using Zeroprox.Domain.Entities;

namespace Zeroprox.Web.Controllers.Skills
{
    [Authorize(Roles = "Admin")]
    [Route("Admin/[controller]")]
    public class SkillsController : Controller
    {
        private readonly ISkillsUnitOfWork _skillsUnitOfWork;

        public SkillsController(ISkillsUnitOfWork skillsUnitOfWork)
        {
            _skillsUnitOfWork = skillsUnitOfWork;
        }

        // GET: Skill
        public IActionResult Index()
        {
            var skills = _skillsUnitOfWork.SkillRepository.GetSkillsWithMasteries();
            return View(skills.ToList());
        }

        // GET: Skill/Details/5
        [HttpGet("Detail/{id:int?}")]
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var skills = _skillsUnitOfWork.SkillRepository.GetById(id);
            var masteryName = _skillsUnitOfWork.MasteryRepository.GetById(skills.MasteryId).Rank;

            ViewData["MasteryRank"] = masteryName;

            if (skills == null)
            {
                return HttpNotFound();
            }

            return View(skills);
        }

        // GET: Skill/Create
        [HttpGet("Create")]
        public IActionResult Create()
        {
            PopulateMasteryDropDownList();
            return View();
        }

        // POST: Skill/Create
        [HttpPost("Create")]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Skill skills)
        {
            if (ModelState.IsValid)
            {
                _skillsUnitOfWork.SkillRepository.Create(skills);
                return RedirectToAction("Index");
            }
            PopulateMasteryDropDownList(skills.MasteryId);
            return View(skills);
        }

        // GET: Skill/Edit/5
        [HttpGet("Edit/{id:int?}")]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var skills = _skillsUnitOfWork.SkillRepository.GetById(id);
            if (skills == null)
            {
                return HttpNotFound();
            }
            PopulateMasteryDropDownList(skills.MasteryId);
            return View(skills);
        }

        // POST: Skill/Edit/5
        [HttpPost("Edit")]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Skill skills)
        {
            if (ModelState.IsValid)
            {
                _skillsUnitOfWork.SkillRepository.Update(skills);
                return RedirectToAction("Index");
            }
            PopulateMasteryDropDownList(skills.MasteryId);
            return View(skills);
        }

        // GET: Skill/Delete/5
        [HttpGet("Delete/{id:int?}"), ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var skills = _skillsUnitOfWork.SkillRepository.GetById(id);
            if (skills == null)
            {
                return HttpNotFound();
            }

            return View(skills);
        }

        // POST: Skill/Delete/5
        [HttpPost("Delete/{id:int}"), ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var skills = _skillsUnitOfWork.SkillRepository.GetById(id);
            _skillsUnitOfWork.SkillRepository.Delete(skills);
            return RedirectToAction("Index");
        }

        private void PopulateMasteryDropDownList(object selectedMastery = null)
        {
            var masteryQuery = _skillsUnitOfWork.MasteryRepository
                .GetAll().OrderBy(d => d.Percent);

            ViewData["MasteryId"] = new SelectList(masteryQuery, "MasteryId", "Rank", selectedMastery);
        }

        protected override void Dispose(bool disposing)
        {
            _skillsUnitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
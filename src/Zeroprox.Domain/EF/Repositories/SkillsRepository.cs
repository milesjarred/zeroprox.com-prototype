﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Data.Entity;
using Zeroprox.Domain.Abstract.IRepositories;
using Zeroprox.Domain.Entities;

namespace Zeroprox.Domain.EF.Repositories
{
    public class SkillsRepository : GenericRepository<Skill>, ISkillsRepository
    {
        public SkillsRepository(ApplicationDbContext context)
            : base(context)
        {
        }

        public ApplicationDbContext ApplicationDbContext
        {
            get { return Context; }
        }

        public IEnumerable<Skill> GetSkillsWithMasteries()
        {
            return ApplicationDbContext.Skills
                .Include(m => m.Mastery)
                .ToList();
        }
    }
}
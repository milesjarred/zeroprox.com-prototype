﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Data.Entity;
using Zeroprox.Domain.Abstract.IRepositories;
using Zeroprox.Domain.Entities;

namespace Zeroprox.Domain.EF.Repositories
{
    public class SocialRepository : GenericRepository<Social>, ISocialRepository
    {
        public SocialRepository(ApplicationDbContext context)
            : base(context)
        {
        }

        public ApplicationDbContext ApplicationDbContext
        {
            get { return Context; }
        }

        public IEnumerable<Social> GetSocialWithFile()
        {
            return ApplicationDbContext.Socials
                .Include(m => m.File)
                .ToList();
        }
    }
}
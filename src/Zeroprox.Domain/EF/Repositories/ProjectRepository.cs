﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Data.Entity;
using Zeroprox.Domain.Abstract.IRepositories;
using Zeroprox.Domain.Entities;

namespace Zeroprox.Domain.EF.Repositories
{
    public class ProjectRepository : GenericRepository<Project>, IProjectRepository
    {
        public ProjectRepository(ApplicationDbContext context)
            : base(context)
        {
        }

        public ApplicationDbContext ApplicationDbContext
        {
            get { return Context; }
        }

        public IEnumerable<Project> GetProjectsWithFiles()
        {
            return Context.Projects
                .Include(m => m.File)
                .ToList();
        }
    }
}
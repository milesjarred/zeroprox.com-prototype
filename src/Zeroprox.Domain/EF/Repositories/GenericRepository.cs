﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Data.Entity;
using Zeroprox.Domain.Abstract.IRepositories;
using Zeroprox.Domain.EF.Extensions;

namespace Zeroprox.Domain.EF.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected readonly ApplicationDbContext Context;

        public GenericRepository(ApplicationDbContext context)
        {
            Context = context;
        }

        // Add an entity
        public void Create(TEntity entity)
        {
            Context.Set<TEntity>().Add(entity);

            Context.SaveChanges();
        }

        // Update an entity
        public void Update(TEntity entity)
        {
            Context.Set<TEntity>().Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;

            Context.SaveChanges();
        }

        // Get an entity by its id
        public TEntity GetById(int? id)
        {
            return Context.Set<TEntity>().Find(id);
        }

        // Get all entites of a specfic type
        public IEnumerable<TEntity> GetAll()
        {
            return Context.Set<TEntity>().ToList();
        }

        // Remove an entity
        public void Delete(TEntity entityToDelete)
        {
            if (Context.Entry(entityToDelete).State == EntityState.Detached)
            {
                Context.Set<TEntity>().Attach(entityToDelete);
            }
            Context.Set<TEntity>().Remove(entityToDelete);

            Context.SaveChanges();
        }
    }
}
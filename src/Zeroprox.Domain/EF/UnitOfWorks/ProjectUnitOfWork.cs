﻿using System;
using Zeroprox.Domain.Abstract.IRepositories;
using Zeroprox.Domain.Abstract.IUnitOfWork;
using Zeroprox.Domain.EF.Repositories;
using Zeroprox.Domain.Entities.Files;

namespace Zeroprox.Domain.EF.UnitOfWorks
{
    public class ProjectUnitOfWork : IProjectUnitOfWork
    {
        private readonly ApplicationDbContext _context;

        private bool _disposed;

        public ProjectUnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            FilesRepository = new GenericRepository<FileUpload>(_context);
            ProjectRepository = new ProjectRepository(_context);
        }

        public IGenericRepository<FileUpload> FilesRepository { get; set; }
        public IProjectRepository ProjectRepository { get; set; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }
    }
}
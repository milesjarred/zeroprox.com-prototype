﻿using System;
using Zeroprox.Domain.Abstract.IRepositories;
using Zeroprox.Domain.Abstract.IUnitOfWork;
using Zeroprox.Domain.EF.Repositories;
using Zeroprox.Domain.Entities;

namespace Zeroprox.Domain.EF.UnitOfWorks
{
    public class SkillsUnitOfWork : ISkillsUnitOfWork
    {
        private readonly ApplicationDbContext _context;

        private bool _disposed;

        public SkillsUnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            MasteryRepository = new GenericRepository<Mastery>(_context);
            SkillRepository = new SkillsRepository(_context);
        }

        public IGenericRepository<Mastery> MasteryRepository { get; }
        public ISkillsRepository SkillRepository { get; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }
    }
}
﻿using System;
using Zeroprox.Domain.Abstract.IRepositories;
using Zeroprox.Domain.Abstract.IUnitOfWork;
using Zeroprox.Domain.EF.Repositories;
using Zeroprox.Domain.Entities.Files;

namespace Zeroprox.Domain.EF.UnitOfWorks
{
    public class SocialUnitOfWork : ISocialUnitOfWork
    {
        private readonly ApplicationDbContext _context;

        private bool _disposed;

        public SocialUnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            FileRepository = new GenericRepository<FileUpload>(_context);
            SocialRepository = new SocialRepository(_context);
        }

        public IGenericRepository<FileUpload> FileRepository { get; }
        public ISocialRepository SocialRepository { get; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }
    }
}
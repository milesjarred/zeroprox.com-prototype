﻿using System;
using Zeroprox.Domain.Abstract.IRepositories;
using Zeroprox.Domain.Entities.Files;

namespace Zeroprox.Domain.Abstract.IUnitOfWork
{
    public interface ISocialUnitOfWork : IDisposable
    {
        IGenericRepository<FileUpload> FileRepository { get; }
        ISocialRepository SocialRepository { get; }
    }
}
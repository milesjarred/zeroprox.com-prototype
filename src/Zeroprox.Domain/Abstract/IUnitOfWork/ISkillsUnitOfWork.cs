﻿using System;
using Zeroprox.Domain.Abstract.IRepositories;
using Zeroprox.Domain.Entities;

namespace Zeroprox.Domain.Abstract.IUnitOfWork
{
    public interface ISkillsUnitOfWork : IDisposable
    {
        IGenericRepository<Mastery> MasteryRepository { get; }
        ISkillsRepository SkillRepository { get; }
    }
}
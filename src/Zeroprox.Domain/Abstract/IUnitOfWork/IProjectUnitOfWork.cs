﻿using System;
using Zeroprox.Domain.Abstract.IRepositories;
using Zeroprox.Domain.Entities.Files;

namespace Zeroprox.Domain.Abstract.IUnitOfWork
{
    public interface IProjectUnitOfWork : IDisposable
    {
        IGenericRepository<FileUpload> FilesRepository { get; set; }
        IProjectRepository ProjectRepository { get; set; }
    }
}
﻿using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;
using Zeroprox.Domain.Entities;

namespace Zeroprox.Domain.Abstract.IRepositories
{
    public interface ISkillsRepository : IGenericRepository<Skill>
    {
        IEnumerable<Skill> GetSkillsWithMasteries();
    }
}
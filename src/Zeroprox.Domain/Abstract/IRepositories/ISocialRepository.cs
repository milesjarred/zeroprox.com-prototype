﻿using System.Collections.Generic;
using Zeroprox.Domain.Entities;

namespace Zeroprox.Domain.Abstract.IRepositories
{
    public interface ISocialRepository : IGenericRepository<Social>
    {
        IEnumerable<Social> GetSocialWithFile();
    }
}
﻿using System;
using System.Collections.Generic;

namespace Zeroprox.Domain.Abstract.IRepositories
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        TEntity GetById(int? id);
        IEnumerable<TEntity> GetAll();

        void Create(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
    }
}
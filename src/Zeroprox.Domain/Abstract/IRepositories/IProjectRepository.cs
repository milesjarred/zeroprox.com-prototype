﻿using System.Collections.Generic;
using Zeroprox.Domain.Entities;

namespace Zeroprox.Domain.Abstract.IRepositories
{
    public interface IProjectRepository : IGenericRepository<Project>
    {
        IEnumerable<Project> GetProjectsWithFiles();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zeroprox.Domain.Entities.Options
{
    public class EmailOptions
    {
        public string SmtpServer { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Zeroprox.Domain.Entities.Files;

namespace Zeroprox.Domain.Entities
{
    public class Social
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "About Me")]
        [DataType(DataType.MultilineText)]
        public string AboutMe { get; set; }

        [Display(Name = "LinkedIn Link")]
        [DataType(DataType.Url)]
        public string LinkedInUrl { get; set; }

        [Display(Name = "Twitter Link")]
        [DataType(DataType.Url)]
        public string TwitterUrl { get; set; }

        [Display(Name = "BitBucket Link")]
        [DataType(DataType.Url)]
        public string BitBucketUrl { get; set; }

        [EmailAddress]
        [Display(Name = "Email")]
        public string ContactEmail { get; set; }

        [Required]
        [Display(Name = "Site Image")]
        public int FileId { get; set; }

        [ForeignKey("FileId")]
        public FileUpload File { get; set; }
    }
}
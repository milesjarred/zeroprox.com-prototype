﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Zeroprox.Domain.Entities
{
    public class Skill
    {
        [Key]
        public int SkillId { get; set; }

        [Required]
        [Display(Name = "Skill Name")]
        public string SkillName { get; set; }

        [Required]
        [Display(Name = "Mastery Level")]
        public int MasteryId { get; set; }

        [ForeignKey("MasteryId")]
        public virtual Mastery Mastery { get; set; }
    }
}
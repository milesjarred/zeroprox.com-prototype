﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.OptionsModel;
using Newtonsoft.Json;

namespace Zeroprox.Domain.Entities.Captcha
{
    public class CaptchaOptions
    {
        public string SiteKey { get; set; }
        public string Secret { get; set; }
    }

    public class ReCaptchaResponse
    {
        private readonly IOptions<CaptchaOptions> _options;

        public ReCaptchaResponse(IOptions<CaptchaOptions> options)
        {
            _options = options;
        }

        [JsonProperty("success")]
        public string Success { get; set; }

        [JsonProperty("error-codes")]
        public ICollection<string> ErrorCodes { get; set; }

        public async Task<string> IsCaptchaValid(string encodedResponse)
        {
            var client = new HttpClient();

            // Holds the secret for google reCaptcha
            var privateKey = _options.Value.Secret;

            // Get response from google
            var reply = await client.GetStringAsync(
                $"https://www.google.com/recaptcha/api/siteverify?secret={privateKey}&response={encodedResponse}");

            var captchaResponse = JsonConvert.DeserializeObject<ReCaptchaResponse>(reply);

            return captchaResponse.Success;
        }
    }
}

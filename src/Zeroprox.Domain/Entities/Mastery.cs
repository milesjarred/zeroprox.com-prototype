﻿using System.ComponentModel.DataAnnotations;

namespace Zeroprox.Domain.Entities
{
    public class Mastery
    {
        [Key]
        public int MasteryId { get; set; }

        [Required]
        public string Rank { get; set; }

        public int Percent { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace Zeroprox.Domain.Entities
{
    public class Education
    {
        [Key]
        public int EducationId { get; set; }

        [Required]
        [Display(Name = "Degree Name")]
        public string DegreeName { get; set; }

        [Required]
        [Display(Name = "College Name")]
        public string SchoolName { get; set; }

        [Required]
        [Display(Name = "Starting Year")]
        [Range(1990, 3000, ErrorMessage = "Please choose the year you first attended this college.")]
        public int YearStart { get; set; }

        [Display(Name = "Do you currently go to this school?")]
        public bool CurrentSchool { get; set; }

        [Display(Name = "Ending Year")]
        [Range(1990, 3000, ErrorMessage = "Please choose the year you last attended this college.")]
        public int? YearEnd { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Zeroprox.Domain.Entities.Files;

namespace Zeroprox.Domain.Entities
{
    public class Project
    {
        [Key]
        public int ProjectId { get; set; }

        [Required]
        [Display(Name = "Project Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Project Link")]
        public string Link { get; set; }

        [Display(Name = "Is this your current project?")]
        public bool InProgress { get; set; }

        [Required]
        [Display(Name = "Project Description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Project Image")]
        public int FileId { get; set; }

        [ForeignKey("FileId")]
        public FileUpload File { get; set; }
    }
}
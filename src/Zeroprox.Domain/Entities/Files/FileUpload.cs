﻿using System.ComponentModel.DataAnnotations;

namespace Zeroprox.Domain.Entities.Files
{
    public class FileUpload
    {
        [Key]
        public int FileId { get; set; }

        [Required]
        [Display(Name = "File Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "File Type")]
        public FileType FileType { get; set; }

        [Required]
        [Display(Name = "File Path")]
        public string FilePath { get; set; }
    }
}
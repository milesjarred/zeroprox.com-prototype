﻿namespace Zeroprox.Domain.Entities.Files
{
    public enum FileType
    {
        Image,
        Pdf,
        WordDoc,
        Other
    }
}